# flux test infra

Flux test infrastructure configurations.

`modules/` dir contains terraform modules for various cloud providers. Flux
cloud integration tests can import these modules to create test infrastructure
to run the tests against.
