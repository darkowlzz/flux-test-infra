# ECR Module

Configuration in this directory creates an ECR instance that's suitable for
flux tests.

## Usage

```hcl
provider "aws" {}

resource "random_pet" "suffix" {}

module "eks" {
    source = "git::https://gitlab.com/darkowlzz/flux-test-infra.git//modules/aws/ecr"

    name = "flux-test-${random_pet.suffix.id}"
}
```
